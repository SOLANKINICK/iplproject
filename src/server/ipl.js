const jsonObj=require('./index.js');

var matchObj = jsonObj.useMatchJson();
var deliObj = jsonObj.useDeliveriesJson();

exports.totCnt = function seasonCount(){
  var hashObj={};
  for(let i=0;i<matchObj.length;i++){
      
      if(hashObj.hasOwnProperty(matchObj[i].season)){
          hashObj[matchObj[i].season]++;
      }
      else hashObj[matchObj[i].season]=1;

  }
  return hashObj;
  //console.log(hashObj);
}

exports.winTeam=function winningTeam(){
  var winnerObj={};
  for(let i=0;i<matchObj.length;i++){

      let x={};
      x=matchObj[i];
     if(winnerObj.hasOwnProperty(x.winner)){
         if(winnerObj[x.winner].hasOwnProperty(x.season)){
             winnerObj[x.winner][x.season]++;
         }
          else{
                 winnerObj[x.winner][x.season]=1;
          }

         }
         else {
             winnerObj[x.winner]={};
             winnerObj[x.winner][x.season]=1;

         }
         //console.log(x.winner);

     }
   return winnerObj;
 }



exports.extraConcededRun = function extraRun(){
  var matchId ={};
  var conceded ={};
  for(let i=0;i<matchObj.length;i++){
      if(matchObj[i].season=='2016'){
              if(!matchId.hasOwnProperty(matchObj[i].id)){
              matchId[matchObj[i].id]=1;
              }
      }
  }
  for(let i=0;i<deliObj.length;i++){
      if(matchId.hasOwnProperty(deliObj[i].match_id)){
          if(conceded.hasOwnProperty(deliObj[i].bowling_team)){
              conceded[deliObj[i].bowling_team]+=Number(deliObj[i].extra_runs);
          }
          else {
              conceded[deliObj[i].bowling_team]=Number(deliObj[i].extra_runs);
          }

      }
      else continue;
  }
  
  return conceded;
}

exports.economicalBowlers = function econBowl(){
 
  var matchId15 = {};
  for(let i=0;i<matchObj.length;i++){
      if(matchObj[i].season=='2015'){
              if(!matchId15.hasOwnProperty(matchObj[i].id)){
              matchId15[matchObj[i].id]=1;
              }
      }
  }

   var bowler = {};
  for(let i=0;i<deliObj.length;i++){
      if(matchId15.hasOwnProperty(deliObj[i].match_id)){
               if(!bowler.hasOwnProperty(deliObj[i].bowler)){
                       bowler[deliObj[i].bowler]={};
                       bowler[deliObj[i].bowler]['boll']=0;
                       bowler[deliObj[i].bowler]['total_runs']=0;
               }
                  if(deliObj[i].wide_runs-deliObj[i].noball_runs===0)
                  {
                    bowler[deliObj[i].bowler]['boll']++;
                  
                  }
                  bowler[deliObj[i].bowler]['total_runs']+=deliObj[i].total_runs-deliObj[i].bye_runs-deliObj[i].legbye_runs;
                    // console.log(bowler[deliObj[i].bowler]['boll']);
                    // console.log(bowler[deliObj[i].bowler]['total_runs']);
                  
                }
               }
              

  
  for(let keys in bowler){
      bowler[keys]['boll']= (bowler[keys]['boll']/6).toFixed(2);
      bowler[keys]['eco']=bowler[keys]['total_runs']/bowler[keys]['boll'];
  
  }
  var sortBowler = [];
for (var keys in bowler) {
  sortBowler.push([keys, bowler[keys]['eco']]);
}
sortBowler.sort(function(a, b) {
  return a[1] - b[1];
});

console.log("Top 10 Economical bowlers are:");
var top = sortBowler.slice(0,10);
return top;
}



exports.tossAndWinMatch = function tossMatch(){
  var toss = {};
  for(let i=0;i<matchObj.length;i++){
    if(!toss.hasOwnProperty(matchObj[i].team1)) 
    toss[matchObj[i].team1]=0;
    if(!toss.hasOwnProperty(matchObj[i].team2)) 
    toss[matchObj[i].team2]=0;
    if(matchObj[i].toss_winner === matchObj[i].winner) 
    toss[matchObj[i].winner]++;
  }
  return toss;
}




