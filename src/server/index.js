//import seasonCount from './ipl.js'
'use strict';
let csvToJson = require('convert-csv-to-json');
 let matchJson = csvToJson.fieldDelimiter(',') .getJsonFromCsv('../data/matches.csv');
let deliveriesJson = csvToJson.fieldDelimiter(',') .getJsonFromCsv('../data/deliveries.csv');
const fs=require('fs')
exports.useMatchJson=()=>{
    return matchJson;
}

exports.useDeliveriesJson=()=>{
    return deliveriesJson;
}


const funDescription = require('./ipl.js')

var totalSeasonCount = funDescription.totCnt();
fs.writeFile('../output/matchesPerYear.json' ,JSON.stringify(totalSeasonCount), (err) => {
    if (err) throw err;
    console.log('Data written to file');
});
var winTeamPerYear = funDescription.winTeam();
fs.writeFile('../output/teamPerYearWin.json' ,JSON.stringify(winTeamPerYear), (err) => {
    if (err) throw err;
    console.log('Data written to file');
});

var conceded = funDescription.extraConcededRun();
fs.writeFile('../output/extraConceded.json' ,JSON.stringify(conceded), (err) => {
    if (err) throw err;
    console.log('Data written to file');
});
var economicalBowler = funDescription.economicalBowlers();
fs.writeFile('../output/economicalBowlers.json' ,JSON.stringify(economicalBowler), (err) => {
    if (err) throw err;
    console.log('Data written to file');
});
var tossWinner = funDescription.tossAndWinMatch();
fs.writeFile('../output/tossandwin.json' ,JSON.stringify(tossWinner), (err) => {
    if (err) throw err;
    console.log('Data written to file');
 });

